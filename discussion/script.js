/* JSON Objects
    - JSON stands for javaScrip object Notation
    - It is a data format used by applications to store and transport data between each other.
    - JavaScript objects are not to be confused with JSON.
    - JSON is used by javaScript, but also by many other languages.
    - Uses double quotes for property names and strings.

    {
    "city": "Quezon City",
    "Country": "Philippines"
    }

    - JSON Arrays
    "cities": [
        {"city": "Tokyo", "country": "Japan"},
        {"city": "New York", "country": "U.S"},
        {"city": "Manila", "country": "Philippines"}
    ]

    // Step 1
    user = {
        user: "Jino"
    }
    // Step 2 - javaScript Create stringified JSON
    let user = `{
        "user": "Jino"
    }`
    // Step 3 - stringified JSON is handled by the server
    {
        "user": "Jino"
    }


    - JSON Methods
    - JavaScript itself cannot parse JSON data, nor can it fully convert javaScript Objects into JSON 
*/

// JSON Stringify
let batchesArr = [
    {
        batchName: "Batch 153"
    },
    {
        batchName: "Batch 154"
    }
];

let stringifiedObject = JSON.stringify(batchesArr);
console.log(stringifiedObject);
console.log(typeof stringifiedObject);

let receivedData = `[{"batchName": "Batch 153"}, {"batchName": "Batch 154"}]`
let jsObject = JSON.parse(receivedData);
console.log(jsObject);
console.log(typeof jsObject);